// jQuery
var $ = require('jquery');

// slicknav
require('slicknav');

// Bootstrap
// For more options, open: https://github.com/twbs/bootstrap-sass/blob/master/assets/javascripts/bootstrap-sprockets.js
// require('bootstrap/js/transition');
// require('bootstrap/js/alert');
// require('bootstrap/js/button');
// require('bootstrap/js/carousel');
// require('bootstrap/js/collapse');
// require('bootstrap/js/dropdown');
// require('bootstrap/js/modal');
// require('bootstrap/js/tab');
// require('bootstrap/js/affix');
// require('bootstrap/js/scrollspy');
// require('bootstrap/js/tooltip');
// require('bootstrap/js/popover');

module.exports = function () {
    $('body').css('background-color', 'red');
};
