# Font Squirrel Font-face Generator Configuration File
# Upload this file to the generator to recreate the settings
# you used to create these fonts.

{"mode":"expert","formats":["woff","woff2"],"tt_instructor":"default","fix_gasp":"xy","fix_vertical_metrics":"Y","metrics_ascent":"","metrics_descent":"","metrics_linegap":"","add_spaces":"Y","add_hyphens":"Y","fallback":"custom","fallback_custom":"100","options_subset":"advanced","subset_custom":"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ\u00e4\u00f6\u00fc\u00e0\u00e9\u00e8\u00c4\u00d6\u00dc\u00c0\u00c9\u00c8\/*#@+\u00b0","subset_custom_range":"","subset_ot_features_list":"","ot_features":["ss19"],"css_stylesheet":"stylesheet.css","filename_suffix":"-webfont","emsquare":"2048","spacing_adjustment":"0","rememberme":"Y"}