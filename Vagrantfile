# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/trusty64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.

  # Browsersync default port
  config.vm.network "forwarded_port", guest: 3000, host: 3000

  # Browsersync ui port
  config.vm.network "forwarded_port", guest: 3001, host: 3001

  # Weinre port
  config.vm.network "forwarded_port", guest: 8080, host: 8080

  # ngrok ui
  config.vm.network "forwarded_port", guest: 4040, host: 4040


  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "127.0.0.1"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.box "LemurLabs Website Mockups"
  config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.

  config.vm.synced_folder "./", "/var/www", create: true, group: "www-data", owner: "www-data"


  # Increase boot timeout from 300 to 900 to avoid error:
  #   "Timed out while waiting for the machine to boot. This means that"
  #   "Vagrant was unable to communicate with the guest machine within"
  #   "the configured ("config.vm.boot_timeout" value) time period."
  # Further informations: http://serverfault.com/questions/586059/vagrant-error-connection-timeout-retrying
  config.vm.boot_timeout = 900


  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:


  # Determining OS
  # Sets the CPUs to 1 on Windows because of the error message according to VT-x
  # Set the CPUs to 2 on other OSs :)
  # Further informations: http://stackoverflow.com/questions/26811089/vagrant-how-to-have-host-platform-specific-provisioning-steps
  module OS
    def OS.windows?
        (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
    end

    def OS.mac?
        (/darwin/ =~ RUBY_PLATFORM) != nil
    end

    def OS.unix?
        !OS.windows?
    end

    def OS.linux?
        OS.unix? and not OS.mac?
    end
  end

  config.vm.provider "virtualbox" do |v|
    # Display the VirtualBox GUI when booting the machine
    # vb.gui = true

    v.name = "Noize"

    # Customize the amount of memory on the VM:
    v.memory = "2048"

    if OS.windows? then
      v.cpus = "1"
    else
      v.cpus = "2"
    end

    v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    v.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
  end
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "shell" do |s|
    s.path = "provision/shell/provisioner.sh"
  end
end
