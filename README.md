# Setup development environment

 1. Install [VirtualBox](https://www.virtualbox.org)
 2. Install [Vagrant](https://www.vagrantup.com)

# How to start

 1. run `vagrant up` inside projects root folder
 2. run `vagrant reload` inside projects root folder
 2. run `vagrant ssh` inside projects root folder

# Commands to work with

## Start developing

`gulp serve`

`gulp serve --production`

`gulp serve --public`

What it additionally does:
 1. Creates a publicly accessable URL

Recommended for tests _without_ Browser synchronization.

**Note**: If you want to test the website's performance with _[Google PageSpeed Insights](https://developers.google.com/speed/pagespeed/insights/)_, run `gulp serve --production --public` instead.

`gulp serve --public sync`
Creates a publicly accessable URL _with_ Browser synchronization

Recommended for cross device debugging outside our local network.

### How to create a _very_ stable publicly accessable URL
Open a new terminal and ssh into the vm, then run the following possible commands:

#### `ngrok http 80`
It creates a very stable publicly accessable URL _without_ Browser synchronization.

#### `ngrok http 3000`
It create a very stable publicly accessable URL _with_ Browser synchronization.
