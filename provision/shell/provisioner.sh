#!/bin/bash
echo "Provisioning virtual machine..."

provisioning_time=`date +%s`

# change directory
cd /vagrant

# Prevent stdin and dpkg-preconfigure warnings
# http://serverfault.com/questions/500764/dpkg-reconfigure-unable-to-re-open-stdin-no-file-or-directory
export DEBIAN_FRONTEND=noninteractive

echo "Setting timezone to Europe/Zurich"
timedatectl set-timezone Europe/Zurich

# echo "Updating system"
#     apt-get update # > /dev/null

# echo "Upgrading system"
#     apt-get upgrade -y # > /dev/null

echo "Installing git"
    sudo apt-get install git -y # > /dev/null

echo "Installing node"
    curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash - # > /dev/null
    apt-get install nodejs -y # > /dev/null

echo "Installing global npm modules. This might take some time.."
    sudo npm install -g gulp

# tbd: Update/Install ruby to 2.2
# See: http://stackoverflow.com/questions/18490591/how-to-install-ruby-2-on-ubuntu-without-rvm
echo "Installing/updating ruby 2.2"
    sudo apt-add-repository ppa:brightbox/ruby-ng
    sudo apt-get update
    sudo apt-get install -y ruby2.3 ruby2.3-dev

echo "Installing global gem modules. This might take some time.."
    gem install bundler # > /dev/null
    gem install sass # > /dev/null
    gem install scss_lint # needs ruby >= 2, therefor installing ruby 2.3 previously

echo "Installing local node/ruby modules. This might take some time.."
    if [ ! -d node_modules ]; then
        npm install # > /dev/null
    fi
    if [ ! -d ruby_modules ]; then
        bundle install # > /dev/null
    fi

# http://www.sitepoint.com/vagrantfile-explained-setting-provisioning-shell/
echo "Installing Nginx (webserver)"
    apt-get install nginx -y

# echo "Updating PHP repository"
#     apt-get install python-software-properties build-essential -y # > /dev/null
#     add-apt-repository ppa:ondrej/php5 -y # > /dev/null
#     apt-get update # > /dev/null

# echo "Installing PHP"
#     apt-get install php5-common php5-dev php5-cli php5-fpm -y # > /dev/null

# echo "Installing PHP extensions"
#     apt-get install curl php5-curl php5-gd php5-mcrypt php5-mysql -y # > /dev/null

echo "Configuring Nginx"
    cp /var/www/provision/config/nginx_vhost /etc/nginx/sites-available/nginx_vhost
    ln -s /etc/nginx/sites-available/nginx_vhost /etc/nginx/sites-enabled/
    rm -rf /etc/nginx/sites-available/default
    cp /var/www/provision/config/gzip.conf /etc/nginx/conf.d/
    service nginx restart

# echo "Preparing MySQL"
#     sudo apt-get install debconf-utils -y # > /dev/null
#     debconf-set-selections <<< "mysql-server mysql-server/root_password password 1234"
#     debconf-set-selections <<< "mysql-server mysql-server/root_password_again password 1234"
#     apt-get install mysql-server -y # > /dev/null

# echo "Installing Java for HTML5 validation. This might take some time.."
#     apt-get -qy install openjdk-7-jdk

echo "Installing ngrok (www.ngrok.com)"
    sudo npm install -g ngrok

echo "Setting custom .bashrc file"
    cp provision/bash/.bash_profile /home/vagrant/.bash_profile

provisioning_end_time=`date +%s`
echo Provision execution time was `expr $provisioning_end_time - $provisioning_time` s.
