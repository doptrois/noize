var gutil = require('gulp-util');

module.exports = function (errors) {
    gutil.log(gutil.colors.red("ERROR\n"), errors);
    this.emit("error");
};
