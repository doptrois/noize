var gulp = require('gulp-help')(require('gulp'));
var gutil = require('gulp-util');
var argv = require('yargs').argv;
var localtunnel = require('localtunnel');

gulp.task('create-localtunnel', 'Creates unique publicly accessible url that will proxy all requests to your locally running webserver.', function (callback) {

    // Determine which port to use
    // --public => 80
    // --public sync => 3000
    var port = argv.public == 'sync' ? 3000 : 80;
    var status = port == 3000 ? gutil.colors.green('active') : gutil.colors.red('disabled');

    localtunnel(port, function (err, tunnel) {
        if (!err) {
            console.log(gutil.colors.gray('--------------------------------------------------------------------------------'));
            console.log('Public URL: ' + gutil.colors.magenta(tunnel.url));
            console.log('^ Browser synchronization ' + status);
            // console.log(                    "  ");
            // console.log(gutil.colors.yellow("  For a more stable version, use ngrok instead."));
            // console.log(gutil.colors.green( "  'ngrok http 3000'") + " >> Routes to the Browsersync server.");
            // console.log(gutil.colors.green( "  'ngrok http 80'") + "   >> Routes to the nginx server. (Google PageSpeed ready)");
            console.log(gutil.colors.gray('--------------------------------------------------------------------------------'));
        } else {
            console.log(gutil.colors.red('An error accured:\n') + err);
        }
    }).on('error', function (error) {
        gutil.log(error);
    });

    callback();
});
