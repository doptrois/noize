var gulp = require('gulp-help')(require('gulp'));
var fs = require('fs');
var penthouse = require('penthouse');
var $ = require('gulp-load-plugins')();
var del = require('del');
var runSequence = require('run-sequence');

var criticalPages = [/*{
        url: 'http://localhost/',
        name: 'critical-mobile',
        width: 480,
        height: 600
    }, {
        url: 'http://localhost/',
        name: 'critical-tablet',
        width: 768,
        height: 900
    }, */{
        url: 'http://localhost/',
        name: 'critical',
        width: 1170,
        height: 900
    }];

gulp.task('build-critical', 'Inspects the template(s) to extract the critical css', ['build-critical-delete'], function (callback) {

    criticalPages.map(function (page) {
        penthouse({
            url: page.url,
            css: 'template/dist/stylesheets/main.css',
            width: page.width,
            height: page.height,
            // https://github.com/pocketjoso/penthouse
            forceInclude: [
                '.keepMeEvenIfNotSeenInDom' // example
            ],
            maxEmbeddedBase64Length: 0
        }, function (err, data) {
            // Rebase paths to absolute
            var result = data.replace(/\.{2}\//g, 'template/dist/');
            fs.writeFile('template/dist/stylesheets/' + page.name + '.css', result);
        });
    });

    callback();

});

gulp.task('build-critical-delete'), 'Deteles critical path css files.', function (callback) {
    return del([
        'template/dist/stylesheets/critical.css']
    );
};
