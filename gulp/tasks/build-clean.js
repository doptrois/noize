var gulp = require('gulp-help')(require('gulp'));
var runSequence = require('run-sequence');
var del = require('del');

gulp.task('build-clean', 'Clean build', function (callback) {

    return del([
        'template/dist/*']
    );

    callback();
});
