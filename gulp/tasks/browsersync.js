var gulp = require('gulp-help')(require('gulp'));
var browserSync = require('browser-sync').create('My Server');
var argv = require('yargs').argv;
var compression = require('compression');
var devip = require('dev-ip');

gulp.task('browserSync', 'Starts server to be able to view the website on multiple devices.', ['build'], function (callback) {

    browserSync.init({
        server: {
            baseDir: './',
            middleware: compression() // gzip compression
        },

        // Outputs IP from eth1 instead of eth0
        // If there are problems retrieving the correct IP
        // run 'vagrant reload' outside the vm
        // The external IP should be somehting like this:
        // 192.168.x.x instead of 10.0.2.x
        // If this does not work for you, run:
        // 'ifconfig' inside the vm and try out the listed
        // IPs.
        host: devip()[1],
        logConnections: true,
        reloadOnRestart: true,
        open: false

    }, function (err, bs) {

        // Start localtunnel

        if (argv.public) {
            gulp.start('create-localtunnel');
        }
    });

    callback();
});
