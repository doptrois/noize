var gulp = require('gulp-help')(require('gulp'));
var htmlHint = require('gulp-htmlhint');

gulp.task('html-hint', 'Checks HTML code style.', function () {

    return gulp.src('template/src/html/**/*')
        .pipe(htmlHint('.htmlhintrc'))
        .pipe(htmlHint.reporter('htmlhint-stylish'))
        .pipe(htmlHint.failReporter({ suppress: true }));
});
