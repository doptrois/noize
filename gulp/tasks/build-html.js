var gulp = require('gulp-help')(require('gulp'));
var browserSync = require('browser-sync').get('My Server');
var inlinesource = require('gulp-inline-source');
var argv = require('yargs').argv;
var $ = require('gulp-load-plugins')();

gulp.task('build-html', 'Builds html files', ['html-hint', 'html-validate', 'html-lint'], function () {

    return gulp.src('template/src/html/**/*')

        /**
         * Replaces link (css) references to inline js for basket.js
         * for faster load times:
         * https://regex101.com/r/fS0qS9/1
         * https://regex101.com/r/fS0qS9/2
         * https://github.com/andrewwakeling/basket-css-example
         * main styles
         */
        .pipe($.if(argv.production,
            $.replace(
                /<link(?=[^>]*href=\"([^"]+main)\.css\")(?=[^>]*data-basket-unique=\"([^"]+)\")(?=[^>]*data-basket-key=\"([^"]+)\").*\>/g,
                '<script>basket.require({ url: "$1-localstorage.css", unique: "$2", key: "$3", execute: false }).then(function(responses) {' +
                    'var css = responses[0].data;' +
                    'stylesheet.appendStyleSheet(css, function(err, style) {' +
                        'if (err) console.error(err)' +
                    '});' +
                '})</script>' +
                '<noscript><link rel="stylesheet" href="$1.css"></noscript>'
            )
        ))

        /**
         * Replaces script references to inline js for basket.js
         * for faster load times:
         * https://regex101.com/r/fE1mP1/4
         */
        .pipe($.if(argv.production,
            $.replace(
                /<script(?=[^>]*src=\"([^"]+)\")(?=[^>]*data-basket-unique=\"([^"]+)\")(?=[^>]*data-basket-key=\"([^"]+)\").*<\/script\>/g,
                '<script>basket.require({ url: "$1", unique: "$2", key: "$3" })</script>'
            )
        ))

        // https://github.com/addyosmani/critical-path-css-tools
        .pipe($.if(argv.production, $.inlineSource({
            attribute: 'data-inline',
            rootpath: './'
        })))

        // Inline sources always if attribute is data-inline-always
        // even in dev mode
        .pipe($.inlineSource({
            attribute: 'data-inline-always',
            rootpath: './'
        }))

        .pipe($.htmlmin({
            collapseWhitespace: true,
            collapseInlineTagWhitespace: true,
            removeScriptTypeAttributes: true,
            removeComments: true,
            ignoreCustomComments: [
                /###(.*?)###/
            ]
        }))
        .pipe(gulp.dest('template/dist/html/'))
        .pipe(gulp.dest('./'))
        .pipe($.size())
        .on('end', function () {
                if (browserSync.active) {
                    browserSync.reload();
                }
            }
        );
});
