var gulp = require('gulp-help')(require('gulp'));
var $ = require('gulp-load-plugins')();

gulp.task('html-validate', 'Validates HTML5 code.', ['html-hint'], function () {

    return gulp.src('template/src/html/**/*')
        .pipe($.w3cjs())
        .pipe($.w3cjs.reporter());
});
