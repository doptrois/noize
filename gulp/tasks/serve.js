var gulp = require('gulp-help')(require('gulp'));
var $ = require('gulp-load-plugins')();

gulp.task('serve', 'build-clean > build > browserSync > watch', ['browserSync'], function (callback) {

    $.watch(['template/src/stylesheets/*.scss', 'template/src/stylesheets/**/*.scss'], {usePolling: true}, function (file) {
        $.util.log('Change detected:', $.util.colors.yellow(file.relative));
        gulp.start('build-styles');
    });

    $.watch(['template/src/fonts/*', 'template/src/fonts/**/*', 'template/src/stylesheets/fonts/*'], {usePolling: true}, function (file) {
        $.util.log('Change detected:', $.util.colors.yellow(file.relative));
        gulp.start('build-styles-fonts');
    });

    $.watch(['template/src/images/*', 'template/src/images/**/*'], {usePolling: true}, function (file) {
        $.util.log('Change detected:', $.util.colors.yellow(file.relative));
        gulp.start('build-images');
    });

    $.watch(['template/src/html/**/*'], {usePolling: true}, function (file) {
        $.util.log('Change detected:', $.util.colors.yellow(file.relative));
        gulp.start('build-html');
    });

    gulp.start('build-scripts-watch');

    callback();
});
