var gulp = require('gulp-help')(require('gulp'));
var argv = require('yargs').argv;
var runSequence = require('run-sequence');

gulp.task('build', 'Cleans build and builds the whole project', function (callback) {

    if (argv.production) {
        runSequence(
            'build-clean',
            ['build-scripts', 'build-styles', 'fonts-fa', 'build-images'],
            'build-html',
            'build-critical',
            'build-html',
            callback);
    } else {
        runSequence(
            'build-clean',
            ['build-scripts', 'build-styles', 'fonts-fa', 'build-images'],
            'build-html',
            callback);
    }
});
