var gulp = require('gulp-help')(require('gulp'));
var del = require('del');
var argv = require('yargs').argv;
var browserSync = require('browser-sync').get('My Server');
var fs = require('fs');
var runSequence = require('run-sequence');
var $ = require('gulp-load-plugins')();

gulp.task('build-styles', 'Compiles SCSS files.', ['styles-lint'], function (callback) {

    // Delete existing CSS files
    del(['template/dist/stylesheets/*']);

    // Check if stylesheets directory exists -> for critical.css
    var dir = './template/dist/stylesheets';

    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }

    // Create empty critical.css file
    fs.writeFile('template/dist/stylesheets/critical.css', ' ');

    // Compile SCSS files
    return gulp.src('template/src/stylesheets/main.scss')
        .pipe($.sourcemaps.init())
        .pipe($.sass({ precision: 10 }).on('error', $.sass.logError))

        // Settings for Autoprefixer according to bootstraps recommendation in:
        // https://github.com/twbs/bootstrap-sass#sass-autoprefixer
        .pipe($.autoprefixer({
            browsers: [
                'Android 2.3',
                'Android >= 4',
                'Chrome >= 20',
                'Firefox >= 24',
                'Explorer >= 8',
                'iOS >= 6',
                'Opera >= 12',
                'Safari >= 6'
            ],
            remove: true
        }))
        .pipe($.cssBase64({
            extensionsAllowed: ['.woff']
        }))
        .pipe($.if(argv.production, $.csso()))
        .pipe($.if(!argv.production, $.sourcemaps.write('./')))
        .pipe(gulp.dest('template/dist/stylesheets/'))
        .pipe($.size())
        .pipe($.replace(/\.{2}\//g, 'template/dist/'))
        .pipe($.if(!argv.production && browserSync.active,
            browserSync.stream({match: '**/*.css'})
        ))
        .pipe($.rename({
            suffix: '-localstorage'
        }))
        .pipe(gulp.dest('template/dist/stylesheets/'))
        .pipe($.if(!argv.production && browserSync.active,
            browserSync.stream({match: '**/*.css'})
        ))
        .on('end', function () {
            if (argv.production) runSequence('build-critical', 'build-html');
        });

    callback();
});
