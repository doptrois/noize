var gulp = require('gulp-help')(require('gulp'));
var browserSync = require('browser-sync').get('My Server');
var $ = require('gulp-load-plugins')();

gulp.task('build-images', 'Optimizes images', function () {
    return gulp.src('template/src/images/**/*')
        .pipe($.imagemin({
            progressive: true, // jpeg
            // svgoPlugins: [{/* options here */}], // svg
            // optimizationLevel: 3 // png
            interlaced: true // gif
        }))
        .pipe(gulp.dest('template/dist/images/'))
        .pipe($.if(browserSync.active, browserSync.stream()));
});
