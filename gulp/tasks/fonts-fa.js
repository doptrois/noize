var gulp = require('gulp-help')(require('gulp'));

gulp.task('fonts-fa', 'Moves FontAwesome fonts to dist folder.', function () {

    return gulp.src('node_modules/font-awesome/fonts/*')
        .pipe(gulp.dest('template/dist/fonts/'));
});
