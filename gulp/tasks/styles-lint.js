var gulp = require('gulp-help')(require('gulp'));
var $ = require('gulp-load-plugins')();

gulp.task('styles-lint', 'Tests SCSS syntax standards defined in .scss-lint.yml', function () {
    return gulp.src('template/src/stylesheets/**/*.scss')
        .pipe($.scssLint({
            'config': '.scss-lint.yml',
            //'bundleExec': true, // uncommented -> performance issues
            //'endless': true,
            'maxBuffer': 1024000
        }))
        .pipe($.scssLint.failReporter());
});
