var gulp = require('gulp-help')(require('gulp'));
var argv = require('yargs').argv;
var browserSync = require('browser-sync').get('My Server');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var $ = require('gulp-load-plugins')();

// add custom browserify options here
var customOpts = {
    entries: ['./template/src/javascripts/main.js'],
    debug: !argv.production ? true : false
};

var opts = customOpts;
var b = browserify(opts);

gulp.task('build-scripts', bundle); // so you can run `gulp js` to build the file
b.on('log', $.util.log); // output build logs to terminal

function bundle () {
    return b.bundle()
        // log errors if they happen
        .on('error', $.util.log.bind($.util, 'Browserify Error'))
        .pipe(source('bundle.js'))
        // optional, remove if you don't need to buffer file contents
        .pipe(buffer())
        // optional, remove if you dont want sourcemaps
        .pipe($.if(!argv.production, $.sourcemaps.init({loadMaps: true}))) // loads map from browserify file
            // Add transformation tasks to the pipeline here.
            .pipe($.if(argv.production, $.uglify()))
            .on('error', $.util.log)
        .pipe($.if(!argv.production, $.sourcemaps.write('./'))) // writes .map file
        .pipe(gulp.dest('./template/dist/javascripts'))
        .pipe($.if(browserSync.active, browserSync.reload));
}
