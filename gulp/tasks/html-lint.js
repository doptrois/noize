var gulp = require('gulp-help')(require('gulp'));
var htmlLint = require('gulp-htmllint');

gulp.task('html-lint', 'Checks HTML code style.', function () {

    return gulp.src('template/src/html/**/*')
        .pipe(htmlLint());
});
